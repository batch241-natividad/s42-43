const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController")
const auth = require("../Auth");

router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.get ("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
if (userData && userData.isAdmin === true) {
	userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.status(401).send("Only admins can retrieve products");
	}
});

//Route for User Registration
router.post ("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for User Authentication

router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/details", auth.verify, (req, res) => {

	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);


	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));

});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getUser({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


router.post("/createOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	
	if (userData && userData.isAdmin === false) {

		let data = {
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId,
			totalAmount: req.body.totalAmount
		};
		userController.checkout(data).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.status(401).send("Only admins can archive products");
	}
});


router.put("/:userId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData && userData.isAdmin === true) {
		userController.setAsAdmin(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		})
	} else {
		res.status(401).send("Only admins can set admins");
	}
})

module.exports = router;