const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController")
const auth = require("../Auth");

router.get ("/all", (req, res) => {

	const productData = auth.decode(req.headers.authorization);
if (productData && productData.isAdmin === true) {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
	} else {
		res.status(401).send("Only admins can retrieve products");
	}
});

router.post("/create", (req, res) => {
	const productData = auth.decode(req.headers.authorization);
	
	if (productData && productData.isAdmin === true) {
		productController.addProduct(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.status(401).send("Only admins can create products");
	}
});

router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


router.put("/:productId/edit", auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization);
	
	if (productData && productData.isAdmin === true) {
		productController.updateProduct(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.status(401).send("Only admins can update products");
	}
});

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization);
	
	if (productData && productData.isAdmin === true) {
		productController.updateActiveProduct(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	} else {
		res.status(401).send("Only admins can archive products");
	}
});

module.exports = router;