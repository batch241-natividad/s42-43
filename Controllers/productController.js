const Product = require("../models/Products");
const auth = require("../Auth");

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;

		
	})
} 

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
};

module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};



module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {

		if(error){
			return false;
		} else {
			let message = Promise.resolve('Product has been updated')
		return message.then((value) => {
			return {value};
		})

		};
	});
};

module.exports.updateActiveProduct = (reqParams, reqBody) => {
	return Product.findByIdAndUpdate(reqParams.productId, {isActive:reqBody.isActive}).then((course, error) => {
		if(error){
			return false;
		} else {
			let message = Promise.resolve('Product has been archived')
		return message.then((value) => {
			return {value};
		})

		}
	})

	}


