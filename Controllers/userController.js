const User = require("../models/User");
const Product = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../Auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email:reqBody.email}).then( result => {

		if(result.length > 0) {
			return true
	} else {
		return false
	}
})
}

module.exports.getAllOrders = () => {
	return User.find({"orders.0": { $exists: true } }).then(result => {
		return result;
	});
};


module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	console.log(newUser);
	return newUser.save().then((user, error) => {

		if (error) {
			return false
		} else {
			return user
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		console.log(result)

		if(result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getUser = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";

		
		return result;

	});

};



module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({
			productId: data.productId,
			purchasedOn: new Date(),
			totalAmount: data.totalAmount
		});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			}
			return true;
		});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({
			orderId: data.userId,
			purchasedOn: new Date(),
			totalAmount: data.totalAmount
		});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	if (isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	};
};


module.exports.setAsAdmin = (reqParams, reqBody) => {
	return User.findByIdAndUpdate(reqParams.userId, {isAdmin:reqBody.isAdmin}).then((course, error) => {
		if(error){
			return false;
		}else {
			let message = Promise.resolve('User has been made to admin.')
		return message.then((value) => {
			return {value};
		})

		}
	});
};

