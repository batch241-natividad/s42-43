const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const productRoute = require("./routes/productRoute")
const userRoute = require("./routes/userRoute");

const port = 8080;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.svvw27f.mongodb.net/Capstone-project-natividad?retryWrites=true&w=majority", {

	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We are connected to the Atlas"))

app.use("/users", userRoute);
app.use("/products", productRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})
