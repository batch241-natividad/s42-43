	const mongoose = require("mongoose");

	const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},

		mobileNo: {
			type: String,
			required: [true, "mobileNo is required"]
		},


		isAdmin: {		
			type: Boolean,
			default: false
		},


		orders: [{

		products: [
			{ 
				productName: {
					type: String,
					required: [true, "productName is required"]
				},

				quantity: {
					type: Number,
					default: [true, "quantity is required"]
				}
			}],

		totalAmount: {
			type: Number,
			required: [true, "totalAmount is required"]
		},

		purchasedOn: {
			type: Date,
			required: new Date
		}
		
	}]
	
})


	module.exports = mongoose.model("User", userSchema);
